# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *


class PyStompPy(PythonPackage):
    """Python client library for accessing messaging servers
    (such as ActiveMQ, Artemis or RabbitMQ) using the STOMP
    protocol (STOMP v1.0, STOMP v1.1 and STOMP v1.2)"""

    homepage = "https://github.com/jasonrbriggs/stomp.py"
    pypi     = "stomp.py/stomp.py-8.0.0.tar.gz"

    maintainers = ['haralmha']

    version('8.1.0',                                                        sha256='b4737d002684639753fbf12dedecb8aa85e5e260749c947acd49c482db42a594')
    version('8.0.1',                                                        sha256='d2bc55b4596604feb51d56895d93431ff8ee159b31b28d9038a2310777bbd3fa')
    version('8.0.0',                                                        sha256='7085935293bfcc4a112a9830513275b2e0f3b040c5aad5ff8907e78f285b8b57')
    version('7.0.0',                                                        sha256='fb301f338292b1b95089c6f1d3a38a9dd8353a5ff3f921e389dfa5f9413b5a8a')
    version('6.1.1',                                                        sha256='dbd069175772fa77218fa6d813ffbf6f7856fa0360fa195c465f885658b764e8')
    version('6.1.0',                                                        sha256='1f6c7e1e5089b1d8a75161e66533cabb9895de5121cc3900cb7e12c41c1bda18')
    version('6.0.0',                                                        sha256='4e639fee9aaa288e98b0bb981029a11eb8464276b8d19b3e61adead52bc3c01b')

    depends_on('python@3.6.3:', type=('build', 'run'))
    depends_on('py-poetry@0.12:', type='build')
    depends_on('py-docopt@0.6.2:', type=('build', 'run'))
