# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *


class PyPydantic(PythonPackage):
    """Data validation and settings management using Python type hinting."""

    homepage = "https://github.com/samuelcolvin/pydantic"
    pypi     = "pydantic/pydantic-1.8.2.tar.gz"

    version('1.10.4', sha256='b9a3859f24eb4e097502a3be1fb4b2abb79b6103dd9e2e0edb70613a4459a648')
    version('1.10.3', sha256='01d450f1b6a642c98f58630e807f7554df0a8ce669ffaff087ce9e1fd4ff7ec8')
    version('1.10.2', sha256='91b8e218852ef6007c2b98cd861601c6a09f1aa32bbbb74fab5b1c33d4a1e410')
    version('1.10.1',  sha256='d41bb80347a8a2d51fbd6f1748b42aca14541315878447ba159617544712f770')
    version('1.10.0', sha256='e13788fcad1baf5eb3236856b2a9a74f7dac6b3ea7ca1f60a4ad8bad4239cf4c')
    version('1.9.2', sha256='8cb0bc509bfb71305d7a59d00163d5f9fc4530f0881ea32c74ff4f74c85f3d3d')
    version('1.9.1', sha256='1ed987c3ff29fff7fd8c3ea3a3ea877ad310aae2ef9889a119e22d3f2db0691a')
    version('1.9.0', sha256='742645059757a56ecd886faf4ed2441b9c0cd406079c2b4bee51bcc3fbcd510a')
    version('1.8.2', sha256='26464e57ccaafe72b7ad156fdaa4e9b9ef051f69e175dbbb463283000c05ab7b')

    depends_on('python@3.6.1:', type=('build', 'run'))
    depends_on('py-setuptools', type='build')
    depends_on('py-dataclasses@0.6:', when='^python@:3.6', type=('build', 'run'))
    depends_on('py-typing-extensions@3.7.4.3:', type=('build', 'run'))
