# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *


class PyJsonpatch(PythonPackage):
    """Library to apply JSON Patches according to RFC 6902"""

    homepage = "https://github.com/stefankoegl/python-json-patch"
    pypi = "jsonpatch/jsonpatch-1.23.tar.gz"

    version('1.32', sha256='b6ddfe6c3db30d81a96aaeceb6baf916094ffa23d7dd5fa2c13e13f8b6e600c2')
    version('1.31', sha256='ae6f3686fc71b53a278a9fdeb91afdc0e523e2f8f8a82cb2f04af90413a8eacb')
    version('1.30', sha256='70f875ced7eb1fc1289860218b085403d075465c915ec45f368aa2da36f37c29')
    version('1.28', sha256='e930adc932e4d36087dbbf0f22e1ded32185dfb20662f2e3dd848677a5295a14')
    version('1.27', sha256='4d08af10d71723b5b2924da6ba90f273a4d1a5c6accfb605eb970cb2f9b29cf9')
    version('1.26', sha256='e45df18b0ab7df1925f20671bbc3f6bd0b4b556fb4b9c5d97684b0a7eac01744')
    version('1.25', sha256='ddc0f7628b8bfdd62e3cbfbc24ca6671b0b6265b50d186c2cf3659dc0f78fd6a')
    version('1.24', sha256='cbb72f8bf35260628aea6b508a107245f757d1ec839a19c34349985e2c05645a')
    version('1.23', sha256='49f29cab70e9068db3b1dc6b656cbe2ee4edf7dfe9bf5a0055f17a4b6804a4b9')

    depends_on('py-setuptools', type='build')
    depends_on('py-jsonpointer@1.9', type=('build', 'run'))
