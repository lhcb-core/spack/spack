# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *


class PyBlosc(PythonPackage):
    """A Python wrapper for the extremely fast Blosc compression library"""

    homepage = "http://python-blosc.blosc.org"
    git      = "https://github.com/Blosc/python-blosc.git"
    pypi     = "blosc/blosc-1.11.1.tar.gz"
    # url    = "https://github.com/Blosc/python-blosc/archive/v1.9.1.tar.gz"

    version('1.11.1', sha256='c22119b27bae1063a697f639028b422d55811b0880c3fc0149cbdea791d0b276')
    version('1.11.0', sha256='c985b8f435dbc49b190fe88947539ed710ad0e9aaaf83778acc506a71ada7bd2')
    version('1.10.6', sha256='55d9d57b85d6eeec010c6c399f2820f96f566dccbc6ddfeefb60501f8e10b548')
    version('1.9.1', sha256='ffc884439a12409aa4e8945e21dc920d6bc21807357c51d24c7f0a27ae4f79b9')

    depends_on('python@3.6:', type=('build', 'run'))
    depends_on('py-setuptools', type='build')
    depends_on('py-scikit-build', type='build')
    depends_on('py-cmake@3.11:', type='build')
    depends_on('py-ninja', type='build')
    # depends_on('c-blosc')  # shipped internally
